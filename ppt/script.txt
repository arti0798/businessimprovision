script
----------------
S1 :



A vary Good AfterNoon to all : 
I m Arti Singh and Me Abhishek Asawale are presenting our paper titled as Integrated Approach of IoT, Big Data and AI (Case Study: Smart Village)
Guided by Prof. M. Suryawanshi


--------------------
S2:

Agenda:



------------------------------
S3:

INTRODUCTION:

"Action without vision is merely passing time;
Vision without action is only daydreaming;
But vision with action can change the world."

said by Nelson Mandela.

Smart Villages believes that people in remote villages in the developing world deserve the same opportunities as everyone else.
Most of India’s population lives in its villages. The youth from villages have been migrating to cities in search of work as 
there are less opportunities for employment in villages. 

They leave a good quality life of village for a poor quality of life in cities. 

This leads to slums & poor hygienic conditions of life for them in cities. 
We need to stop this migration from villages to cities. 
For this we need to create work opportunities in villages & make villages SMART for our citizens.

We can use Big data, AI and IOT’s in  
numerous ways to solve these problems.
i.e.

1. While accesing Real-time data
together with AI.

2. Getting the informative data with the help of Data Science.

3. Use of IOT’s for improvement.

----------------------------------------------------------
S4:

Problems :

Starting with SMART villages we have to  manage the most fundamental issues such as : 


1:  Inadequate infrastructure : Infrastructure assets such as rural roads, tracks, bridges, irrigation schemes, water supplies, schools, health centers 
and markets are needed in rural areas for the local population to fulfill their basic needs and live a social and economic productive life.

2: lack of Internet access : Rural communities around the world lacking broadband access miss out on opportunities for digital transformation, 
including the ability to participate in the digital economy.
Fast and reliable internet connectivity will bring new hope to rural villages.

3:  low-income levels : While dealing with fundamental issues we need to generate more and more job opportunities for the villegers.

4:transportation challenges : Rural mobility has received far less attention from
policy‑makers than urban mobility and there is a serious lack
of conventional transport and of various shared mobility
options that are being deployed in many urban areas. 

5:the education system : Smart school is a concept which uses technologies or some modern equipment in the classrooms which enables in giving better learning experience to the students. This also helps in attracting more students to school and also will help in reducing school dropouts.

------------------------------
S5:

FOOD
*****
At the same time a third of our food is wasted.
With increasing pressure for Farmers to grow crops for both
human and animal consumption.

In Food 5.0, Robert Saik takes us on a journey from the “muscle era” of farming to a future where the convergence of new technologies 
like sensors, robotics, and machine learning make infinite sustainability achievable. 

With the veil lifted on modern agriculture practices, we’ll be inspired to contribute to a culture where farmers can adopt the science 
and tools they need to carry out their mission of feeding the planet.

We can actually get somewhere and help people have a better understanding of what farmers do and why. 
If we think about it, 98% of farms are family farms. Many of these businesses have been in their families for over 100 years. 
Why would one say today’s farms are not sustainable? Farm viability really is of prime importance, for without that, there really is no sustainability. 

------------------
S6:

Empowering Agritech via Big Data Analytics and IoT
***************************************************

Best continuing pressure placed upon the agricultural industry
the projected growth in the world population translates into
the need to increase production efficiencies of crops
by as much as 70 percent from the same land resources to
meet the need without significant impact on the environment.

There are also other factors that can hinder arable Farmers
abilities to meet the increasing demand including global
warming more frequent storms and water shortages and pollution
in soil and water streams issues, which can lead to waste
and reduced yield.


-------------------------------
S7: 

SOLUTION
**********

The solution combines The Internet of Things, Big Data, high
performance Computing and cloud computing Technologies to
create Precision agriculture Services Innovation solving
real-world problems within the agricultural industry.


 IoT are the devices or sensors which generate data. where
 Big data and artificial intelligence are in a mutually exclusive relationship — as one advances, the other benefits. 

As it benefits, it progresses, and the cycle continues to evolve both technologies.


DATA WAREHOUSE : Data warehouse analytics leverages large volumes of disparate data which has been centralized in a single repository, 
known as a data warehouse, for use in data analysis, data discovery, and self-service analytics.

so we need to have data driven approach.

---------------------------------
S8:

Data Mining And Processing
***************************

Data is stored and mind using supercomputers providing the foundation for developing data-driven applications and solutions cutilizing artificial intelligence.

The supercomputers store data from multiple sources such as sensors on the ground for soil and plant analysis drone images and satellite Imaging.

Data mining depends on effective data collection, warehousing, and computer processing.
The Data generated and stored we need to turn raw data into useful data.




----------------------------------
S9:

SMART AGRICULTURE
*******************

The streams of data are the basis for extracting key operational
information about soil condition crop nutrient uptake crop
yield forecasting and estimation of optimum harvesting times
monitoring gas emissions and soil contamination.

---------------------------------
S10:


Mining of important information through the use of machine learning and artificial intelligence
*******************************************

The supercomputer also executes the mining of important information
through the use of machine learning and artificial intelligence
to create applications, which can then be delivered through
a number of channels the most amenable being direct to smartphones of the farmers.
-------------------------------------

S11:
Monitor and control Irrigatation system with mobile app:
**********************************************

The information generated by the applications can be visualized
in a format that enables the farmer to make more informed
decisions or longer-term generate control signals that direct
autonomous vehicles to execute routine tasks such as fertilizer
application herbicide spray or crop harvesting.
----------------------------------------------------
S12:

TACKLE ISSUES 
**************


Appropriately presented information informs farmers of any potential issues
such as a lack of nutrients in certain areas of their land
assets or the early indication of a disease outbreak or provides.
Nuts when the crops are ready to harvest.

--------------------------------------------------------

S13:

THE PROCESS
**************

Data is entered from one of the many sources of data collection.The process is as follows data input pre-processing or cleansing of data.
The data is structured and bad bits of data a deleted development
and validation of algorithms visualizations and results interpretation
insights gained and improvements in performance with the
Business model development.

----------------------------------------------------

S14:

OPTIMUM HARVESTING
*********************

The data collected from Precision farming Technologies can
be visualized in interpreted to surface Trends and provide
insights on the optimum Solutions and segmentation of the tasks best carried out by autonomous vehicles.
 
Utilize weather forecasts satellite and drone imagery to
produce more accurate crop growth models for both common
and vulnerable crops and fruits the enhanced information
owing to a more granular integration of local weather and imagery will not only increase the quality and yield of.

But also decrease operational costs the farming Community has
followed traditional practices proven to be efficient in meeting postum.

------------------------------------
S15:

Additional Approaches
***********************

1. Support from Angle Investors, Individual , NGO's and Government in this sectors.

2. Encouraging and motivating eco-tourism, agro-tourism to create job opportunities.

3. We should furnish extra data with legitimate direction such as meeting, video's and article to assist farmers with various cultivating techniques.
We should organize farming campaigns and motivate our "Youth" to take part effectively.

4. We can utilize innovation to capacity and refine water in the event of precipitation.

5. We should direct survey for every region to know the diverse climatic condition, water level, soil moisture and various 
offices to amplify the benefit and increase the production.

6. A non-agricultural land is an infertile land, unsuitable for development and in the event that we own an agrarian land 
and need to raise a structure for private or modern or business purposes by transformation, it is conceivable by Law.



con.........
--------------------------------------------------------
S16:

COLLABORATION OF DIFFERENT sectors
**********************************

COLLABORATION of different sectors like education, Farming, Industries and Technologies bring the great efforts towards SMART VILLAGE.

con.......
-------------------------------
S17:

Smart Village Transition
*************************

We need to join the "connected village" and "model village" to make a smart village 
which needs Infrastructure upgrade with a collection of data and monitoring.

con.....
-----------------------------------------
S18:

Dhanora
***********

It is possible to overcome difficulties with the help of some points: 

a. Mind the investment Gap.
b. Level the playing field.
c. Overcome Non-Market Barriers.
d. Stay Up to Date.

Many villages were adopted, and their effect on the community's growth was positive. e.g.-

Smart Village the Social Outcomes at Dhanora

1. The village has been designated as a "Crime Free Village" by the District Police since no FIR has been filed with the police station.
2. The village is transforming into an Alcohol-free village.
3. Village get awarded by Government.

Nearby 100 villages have been influenced by Dhanora's smart village and have joined the "Soch Badlo Goan Badlo" campaign for rural reform in India. 



------------------------------------
S19:

Villages of India Adopted by Indian American to convert into smart Village in 1000 days
*******************************************************************************************

The soul of India lives in its villages - Mahatma Gandhi


Government of India, under the energetic, committed & innovative leadership of Prime Minister Narendra Modi, 
is working on Smart cities & Smart Villages program, which is good initiative.
The approach is to motivate Indian diaspora living abroad to adopt their home villages in India to be then converted into a smart village in 1000 days. 
The main problem was How to do this ?

So for this - 
They First prepare & submit a secondary research report on the “As is situation” of the village on 70 parameters of village benchmark:

Based on this report, They expect the village adopter from abroad to give us three days of their time whenever they visit India.
During these three days, in their presence, we will conduct a primary survey of village children, youth, middle aged men & women and elderly people 
in the village to find out their PAIN POINTS in the village which they wish to change. 
This is done through separate questionnaires which they have prepared for each category of people, in different languages.

---------------------------------------------------
S20:

Future Thinker
****************

We have many solutions to make our village more strong and smart : 

1. Conceptualize the system for "Smart Village".
2. Build a model to draw an execution system.
3. Plan innovation arrangements, strategy for better and enabled provincial administration.
4. Plan for speculation and asset distribution system for actualizing smart town activity and many more.

And these things are only possible when we actively use the new technology with the help of big data,AI and IoT.

---------------------------------------------------------
S21:

Solutions other than Big Data, IoT and AI
*********************************************

SDG Approach:
Rural villages have emerged as a critical priority for achieving the SDGs (Sustainable Development Goals). 
Since SDG focuses in all aspects we need to follow SDG approach.
SDG Approach mainly focuses in those sectors in which rural areas lack
And SDG 5 is Important in sustainable development since giving woman’s equal right will improve the speed of development and Women's participation in climate change mitigation would serve to ensure that future generations have access to clean air, healthy 
drinking water, adequate food, and safe housing.

Corporate Social Responsibility:
Developing a smart village cannot be achieved just by improving the infrastructure, making available the Internet, etc. 
we need the involvement of government and NGO’s. It will be a major factor for developing Smart Village.
We need Gov policies and NGO’s support as well as Angle Investors, industry involvement.

Labour :
The evidence shows that technological advancements have reduced the need for repetitive mechanised work while 
increasing demand and pay for high-skilled technical and analytical work.

--------------------------------------------------------
S22:

CONCLUSION
***********

We have to make sure that to make villages smart we have to put technology first agenda everywhere. Connect every single citizen of this nation digitally.

We ought to gather different guidance meeting and educate villagers that how this new technology work and benefits.

It will come with great price tag, but it will do the work for the next generations to come and 
there should not be language barriers for villagers to recognise the IOT.

Everyone deserves to live with technology and move with technology. 
No people should be untouched by technology and this can be done with help of:

Analyse and plan.
Design and Develop.
Deploy and Implement.
Monitor and Evaluate.

The growth to 10 billion inhabitants on Earth by 2050 is placing significant demands on the agricultural sector. 
Consequently farmers are beginning to be more receptive
to the use of technology which supports them to meet this
demand and need in a safe and efficient way without 
traditional practices nor compromising quality
of life or the environment




---------------------------




















